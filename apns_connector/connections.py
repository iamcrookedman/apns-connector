import asyncio
import json
from ssl import SSLContext

import OpenSSL
from .exceptions import APNsError, APNsDisconnectError, HTTP2Error, DisconnectError
from .protocols import H2ClientProtocol, HTTPMethods

DEV_APNS_SERVER = 'api.development.push.apple.com'
TEST_APNS_SERVER = 'api.push.apple.com'


class APNsConnection:
    def __init__(self, cert_file, loop=None, use_sandbox=False):
        self.protocol = None
        self.cert_file = cert_file
        self.host = DEV_APNS_SERVER if use_sandbox else TEST_APNS_SERVER
        self.port = 443
        self.ssl_context = SSLContext()
        self.ssl_context.load_cert_chain(cert_file)

        self.loop = loop or asyncio.get_event_loop()

        with open(self.cert_file, 'rb') as f:
            body = f.read()
            cert = OpenSSL.crypto.load_certificate(
                OpenSSL.crypto.FILETYPE_PEM, body
            )
            self.apns_topic = cert.get_subject().UID

    @property
    def connected(self):
        return self.protocol is not None and self.protocol.connected

    async def connect(self):
        _, self.protocol = await self.loop.create_connection(
            protocol_factory=lambda: H2ClientProtocol(self.loop),
            host=self.host,
            port=self.port,
            ssl=self.ssl_context
        )

    def disconnect(self):
        if self.connected:
            self.protocol.transport.close()
        self.protocol = None

    def _prepare_request(self, payload, token):
        data = json.dumps(payload.data).encode()
        request_headers = [
            (':method', HTTPMethods.POST),
            (':scheme', 'https'),
            (':path', '/3/device/{}'.format(token)),
            ('host', self.host),
            ('content-length', str(len(data))),
            ('apns-topic', self.apns_topic),
        ]
        return request_headers, data

    async def send_message(self, payload, token):
        if not self.connected:
            await self.connect()
        headers, data = self._prepare_request(payload, token)
        try:
            headers, _ = await self.protocol.send_request(headers, data)
            print(headers)
            return headers.get('apns-id')
        except HTTP2Error as exc:
            error_data = exc.json_data()
            reason = error_data.get('reason', None) if error_data else None
            raise APNsError(reason, exc.headers.get('apns-id'))
        except DisconnectError as exc:
            error_data = exc.json_data()
            reason = error_data.get('reason', None) if error_data else None
            raise APNsDisconnectError(reason)
