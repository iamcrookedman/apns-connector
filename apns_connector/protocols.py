import asyncio
import collections
from typing import Dict

from .exceptions import DisconnectError, HTTP2Error
from h2.connection import H2Connection, ConnectionState
from h2.events import ResponseReceived, DataReceived, WindowUpdated, StreamEnded, ConnectionTerminated
from h2.exceptions import NoAvailableStreamIDError


class HTTPMethods:
    GET = 'GET'
    POST = 'POST'


class H2ClientProtocol(asyncio.Protocol):
    def __init__(self, loop=None):
        self.response_futures = dict()  # type: Dict[int, asyncio.Future]
        self.flow_control_futures = dict()  # type: Dict[int, asyncio.Future]

        self.stream_waiters = collections.deque()
        self.events_queue = collections.defaultdict(collections.deque)  # type: Dict[int, collections.deque]

        self.conn = H2Connection()
        self.transport = None
        self.loop = loop or asyncio.get_event_loop()

    def connection_made(self, transport):
        self.transport = transport
        self.conn.initiate_connection()
        self.transport.write(self.conn.data_to_send())

    def connection_lost(self, exc):
        self._on_terminated(None, None)
        self.transport = None

    def data_received(self, data):
        events = self.conn.receive_data(data)
        self.transport.write(self.conn.data_to_send())
        for event in events:
            if isinstance(event, ResponseReceived) or isinstance(event, DataReceived):
                self.events_queue[event.stream_id].append(event)
            elif isinstance(event, WindowUpdated):
                self._on_window_updated(event)
            elif isinstance(event, StreamEnded):
                self.events_queue[event.stream_id].append(event)
                self._handle_response(event.stream_id)
                self._on_stream_closed()
            elif isinstance(event, ConnectionTerminated):
                self._on_terminated(event.error_code, event.additional_data)
        self.transport.write(self.conn.data_to_send())

    @property
    def connected(self):
        return self.transport is not None and not self.conn.state_machine.state == ConnectionState.CLOSED

    def _on_terminated(self, error_code, data):
        while self.response_futures:
            _, future = self.response_futures.popitem()
            future.set_exception(DisconnectError(error_code, data))
        while self.stream_waiters:
            future = self.stream_waiters.popleft()
            future.set_exception(DisconnectError(error_code, data))

    def _on_window_updated(self, event):
        if event.stream_id:
            if event.stream_id in self.flow_control_futures:
                future = self.flow_control_futures.pop(event.stream_id)
                future.set_result(None)
            else:
                for future in self.flow_control_futures.values():
                    future.set_result(None)
                self.flow_control_futures = dict()

    def _on_stream_closed(self):
        if self.stream_waiters:
            future = self.stream_waiters.popleft()
            future.set_result(None)

    def _handle_response(self, stream_id):
        response_future = self.response_futures.pop(stream_id)
        response_event = self.events_queue[stream_id].popleft()

        event_data = None
        while True:
            data = self.events_queue[stream_id].popleft()
            if isinstance(data, DataReceived):
                event_data = data
            elif isinstance(data, StreamEnded):
                break
        self.events_queue.pop(stream_id)

        headers = {key.decode(): value.decode() for key, value in response_event.headers}
        data = event_data.data if event_data else None
        status_code = int(headers[':status'])
        if status_code != 200:
            error = HTTP2Error(status_code, headers, data)
            response_future.set_exception(error)
        else:
            response_future.set_result((headers, data))

    async def _send_request_body(self, stream_id, body):
        while True:
            window_size = self.conn.local_flow_control_window(stream_id)
            available_window = min(window_size, len(body))
            data_for_send = body[:available_window]
            body = body[available_window:]

            chunk_size = self.conn.max_outbound_frame_size
            chunks = (data_for_send[i: i + chunk_size] for i in range(0, len(data_for_send), chunk_size))

            for chunk in chunks:
                self.conn.send_data(stream_id, chunk)
            self.transport.write(self.conn.data_to_send())

            if body:
                future = asyncio.Future(loop=self.loop)
                self.flow_control_futures[stream_id] = future
                await future
            else:
                break

    async def _send_request(self, stream_id, headers, body):
        self.conn.send_headers(stream_id, headers)
        future = asyncio.Future(loop=self.loop)
        self.response_futures[stream_id] = future

        if body:
            await self._send_request_body(stream_id, body)
        self.conn.end_stream(stream_id)
        self.transport.write(self.conn.data_to_send())

        return await future

    async def send_request(self, headers, body=None):
        while True:
            try:
                stream_id = self.conn.get_next_available_stream_id()
                future = self._send_request(stream_id, headers, body)
                return await future
            except NoAvailableStreamIDError:
                wait_feature = asyncio.Future(loop=self.loop)
                self.stream_waiters.append(wait_feature)
                await wait_feature
