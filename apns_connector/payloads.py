class Payload:
    def __init__(self, **kwargs):
        self.title = kwargs.get('title')
        self.category = kwargs.get('category')
        self.body = kwargs.get('body')
        self.metadata = kwargs.get('metadata')
        self.badge = kwargs.get('badge', 0)

    @property
    def data(self):
        message = {
            'aps': {
                'alert': {
                    'title': self.title,
                    'body': self.body
                },
                'mutable-content': 1,
                'content-available': 1,
                'category': self.category,
                'sound': 'default',
                'badge': self.badge,
            },
            'custom': self.metadata
        }

        return message
