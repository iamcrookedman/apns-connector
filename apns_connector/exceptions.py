import json


class JSONDataMixin:
    data = None

    def json_data(self):
        if self.data:
            try:
                return json.loads(self.data.decode())
            except json.JSONDecodeError:
                pass
        return {}


class HTTP2Error(Exception, JSONDataMixin):
    def __init__(self, code, headers, data=None):
        self.code = code
        self.headers = headers
        self.data = data


class DisconnectError(Exception, JSONDataMixin):
    def __init__(self, code, data=None):
        self.code = code
        self.data = data


class APNsError(Exception):
    def __init__(self, reason, identifier):
        super().__init__()
        self.reason = reason
        self.identifier = identifier

    def __repr__(self):
        return 'APNsError(reason={}, identifier={})'.format(self.reason, self.identifier)

    def __str__(self):
        return "APNsError({})".format(self.reason)


class APNsDisconnectError(Exception):
    def __init__(self, reason):
        super().__init__()
        self.reason = reason

    def __repr__(self):
        return 'APNsError(reason={})'.format(self.reason)

    def __str__(self):
        return "APNsError({})".format(self.reason)
