from contextlib import contextmanager

from .connections import APNsConnection


@contextmanager
def create_apns_connection(cert_file, loop):
    apns_connection = APNsConnection(cert_file=cert_file, loop=loop, use_sandbox=True)
    yield apns_connection
    apns_connection.disconnect()


class APNSConnectionManager:
    __connection = None

    @staticmethod
    def get_connections(cert_file, loop, use_sandbox=True):
        if APNSConnectionManager.__connection is None:
            APNSConnectionManager.__connection = APNsConnection(cert_file=cert_file, loop=loop, use_sandbox=use_sandbox)
            print('start apns connection')
        return APNSConnectionManager.__connection

    @staticmethod
    def close_session():
        if APNSConnectionManager.__connection:
            APNSConnectionManager.__connection.disconnect()
            APNSConnectionManager.__connection = None
        print('close apns connection')
