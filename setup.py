from setuptools import setup

setup(
    name='apns-connector',
    version='0.0.6',
    platforms='all',
    author='Dmitry Krivokhizhin',
    author_email='kd8433@gmail.com',
    url='https://bitbucket.org/iamcrookedman/apns-connector/',
    description='asyncio client for apns',
    packages=['apns_connector'],
    install_requires=[
        'h2==3.0.0',
        'pyopenssl==18.0.0',
    ]

)